
name := "build-webjar-example"

organization := "org.shuttj"

version := "0.0.1"

publishArtifact in (Compile, packageDoc) := false

publishArtifact in (Compile, packageSrc) := false

LessKeys.compress in Assets := true

lazy val root = (project in file(".")).enablePlugins(SbtWeb)