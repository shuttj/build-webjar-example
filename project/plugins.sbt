
resolvers += Resolver.typesafeRepo("releases")

addSbtPlugin("com.github.mpeltonen" % "sbt-idea" % "1.5.2")

addSbtPlugin("com.typesafe.sbt" % "sbt-web" % "1.1.0")

addSbtPlugin("com.typesafe.sbt" % "sbt-less" % "1.0.2")
